# SymphonyOS by HD Scania: My personal FreeBSD LiveDVD project featuring KDE Plasma, Enlightenment, XFCE, MATE, Trinity TDE, Liri shell, Weston, CDE, and Sway (based on helloSystem and FuryBSD)
Those Live ISO builders build Live ISO’s from FreeBSD `-STABLE` and `-CURRENT` ingredients. It is based on [helloSystem](https://github.com/helloSystem/ISO) by [probonopd](https://github.com/probonopd) and [furybsd-livecd](https://github.com/furybsd/furybsd-livecd/) by Joe Maloney.

## Release

The upcoming release build r0.23.04a01 will be released [at my SourceForge files for this project](https://sourceforge.net/projects/symphony-freebsd/files/). Maybe parts of them will also be released on CodeBerg, Disroot Git, Gitea.com, or both those all.

## My featured LiveDVD environments
### Available on official FreeBSD ports
1. KDE Plasma (my default interface; Wayland and X11; a desktop composition only)
2. Enlightenment (Wayland and X11; desktop and tiling compositions)
3. XFCE (X11 only; a desktop composition only)
4. MATE (X11 only; a desktop composition only)
5. CDE (X11 only; a desktop composition only)
6. Sway (Wayland only; a tiling composition only)

### Requires my own personal port trees
1. [Trinity TDE](https://TrinityDesktop.org) (X11 only; a desktop composition only)
2. [Liri shell](https://liri.io) (Wayland only; a desktop composition only)
3. [Weston](https://gitlab.freedesktop.org/wayland/weston/-/blob/master/README.md) (Wayland only; a desktop composition only)

## Continuous builds
Continuous builds can be downloaded [here](../../releases/). __CAUTION:__ Those are meant for development only. Use at your own risk. Do not use in production environments.

To minimize the amount of data when going from build to build, `.zsync` files are also provided. [More information](https://askubuntu.com/questions/54241/how-do-i-update-an-iso-with-zsync)

It is possible to directly download and move straight to a Ventoy USB stick in one go.

### Git cloning mirrors
1. https://git.code.sf.net/p/symphony-freebsd/13-STABLE (Primary `13-STABLE` LiveDVD tree)
    1. https://notabug.org/HD_Scanius/SymphonyOS_13-STABLE.git
    2. https://gitlab.com/hd_scania/SymphonyOS_13-STABLE.git
2. https://codeberg.org/hd_scania/SymphonyOS_13-STABLE.git (Codeberg `13-STABLE` LiveDVD tree)
    * https://notabug.org/HD_Scanius/SymphonyOS_codeberg_13-STABLE.git
        * https://gitlab.com/hd_scania/SymphonyOS_codeberg_13-STABLE.git
3. https://git.disroot.org/hd_scania/SymphonyOS_13-STABLE.git (Disroot Git `13-STABLE` LiveDVD tree)
    * https://notabug.org/HD_Scanius/SymphonyOS_disroot_13-STABLE.git
        * https://gitlab.com/hd_scania/SymphonyOS_disroot_13-STABLE.git
4. https://gitea.com/hd_scania/SymphonyOS_13-STABLE.git (Gitea.com `13-STABLE` LiveDVD tree)
    * https://notabug.org/HD_Scanius/SymphonyOS_GiteaDotCom_13-STABLE.git
        * https://gitlab.com/hd_scania/SymphonyOS_GiteaDotCom_13-STABLE.git
5. [`https://code.OneDev.io/SymphonyOS_13-STABLE`](https://code.OneDev.io/projects/352) (OneDev.io `13-STABLE` LiveDVD tree)
    * https://notabug.org/HD_Scanius/SymphonyOS_1devio_13-STABLE.git
        * https://gitlab.com/hd_scania/SymphonyOS_1devio_13-STABLE.git
6. https://BitBucket.org/hd_scania/symphonyos_13-stable.git (BitBucket `13-STABLE` LiveDVD tree)
    * https://notabug.org/HD_Scanius/SymphonyOS_BitBucket_13-STABLE.git
        * https://gitlab.com/hd_scania/SymphonyOS_BitBucket_13-STABLE.git
7. [`git@git.sr.ht:~hd_scania/SymphonyOS_13-STABLE`](https://git.sr.ht/~hd_scania/SymphonyOS_13-STABLE) (SourceHut `13-STABLE` LiveDVD tree)
    * SourceHut is **fatally and mortally** mirroring unfriendly, so no mirrors **pulled from or pushed onto** Notabug and GitLab *were, have been, or would be* anymore.
8. https://git.code.sf.net/p/symphony-freebsd/CURRENT (Primary `-CURRENT` LiveDVD tree)

### Why nothing GitHub?
Don’t you still remember they’re `m$`-acquired? How **sucky** their Windows has been, is also **if and only if** how **sucky** their GitHub will **also always** be

## Minimal system recommendations for live media
1. AMD Ryzen 5 3500U or its Intel 4C8T replacements
2. 15 GiB RAM (14.9GiB = decimal 16GB; only showing live media requirements for minimal **physical** RAM’s; for real h/w installing also better with swap partitions 8GiB+)
3. HDMI capable of 1920×1080 screen resolution
4. For ``-STABLE``, an UEFI USB pendrive with [Ventoy](https://ventoy.net) installed, and then put my LiveDVD’s onto the `exfat` partition, for booting the installer media thru Ventoy (The Ventoy founder has told me he only supports FreeBSD LiveDVD’s whose extension is `.iso` but never LiveUSB’s whose extension is `.img`)
5. For ``-CURRENT``, an independently ``dd``’ed LiveDVD media on a whole microSD (The Ventoy founder also has told me he will never work for a single ``-CURRENT`` based medium)

## Credentials for live media
There isn’t a single password for `liveuser`. The `liveuser` account is removed upon install.
There is neither a single root password until it is set in the installer. You can become root using `doas su` or `sudo su`.

## Acknowledgements
Please see https://hellosystem.github.io/docs/developer/acknowledgements.
These builds would not be possible without the infrastructure generously provided by [Cirrus CI](https://cirrus-ci.com/).
